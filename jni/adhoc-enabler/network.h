#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#include <cstdio>
#include "wpa_ctrl.h"
#include <exception>
#include <errno.h>
#include <string>
#include <string.h>
#include <stdexcept>
#include <memory>
#include <sstream>
#include <iostream>
#include <vector>
#include <unistd.h>
using namespace std;

template <typename T>
std::string tostring(T val)
{
    std::ostringstream out_stream;
    out_stream << val ;
    return out_stream.str() ;
}

int custom_stoi(const std::string& str);
bool custom_isdigit(char& ch);
void toupper(string& cmd);
void debug(const string& message);

class Variable_error_exception: public exception{
    string message;
public:
    Variable_error_exception(string name, string value);
    virtual const char* what() const throw();
};

class Errno_exception : public exception{
    string message;
public:
    Errno_exception(string message);
    virtual const char* what() const throw();
};


class WifiManager;

class Network{
friend class WifiManager;
public:
    Network(string ssid);

    void print() const;
    void print_verbose() const;

private:

public:
    string ssid;
    int priority;
    int frequency;
    int mode;//0: infrastructure mode, 1: ibss, 2: access point (AP).
    string key_mgmt;

    string bssid;
    string flags;

private:
    int id;
};
// ************************ wifi manager aka wpa_client
class WifiManager{

public:
    WifiManager(string socket);

    void create_network(Network& network) const;
    void update_network(Network& network) const;
    bool set_network_as_current(const Network& network) const;
    void set_variable(Network network, string variable, string value) const;
    vector<Network> list_networks() const;
    bool ssid_exists(Network& network) const;
    bool bssid_exists(Network& network) const;
    string send_command(const string cmd, void (*msg_cb)(char *msg, size_t len)) const;

    bool is_reply_ok(const string& reply) const;

    ~WifiManager();
private:

    void set_all_network_variables(Network network) const;
    void open_connection();

    string socket;
    wpa_ctrl* ctrl;
    size_t* max_reply_len;
    char* _reply;
};

#endif // NETWORK_H
