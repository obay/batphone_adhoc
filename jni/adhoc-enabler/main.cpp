#include <errno.h>
#include <cstdlib>
#include <exception>
#include <cstring>
#include "wpa_ctrl.h"
#include <iostream>
#include <sys/types.h>
#include <string>
#include <math.h>
#include <pthread.h>
#include "network.h"
//#include "javainterface.h"
using namespace std;

bool connect_to_public_network(string socket){
    Network network("Public Adhoc Network");
    network.mode = 1;
    network.frequency = 2457;
    network.key_mgmt = string("NONE");
    network.priority = 50;

    WifiManager wifi(socket);
    if(wifi.ssid_exists(network))
        wifi.update_network(network);
    else
        wifi.create_network(network);

    wifi.send_command("AP_SCAN 2", NULL);
    return wifi.set_network_as_current(network);
}

bool return_ap_scan_and_priority_to_normal(string socket){
    WifiManager wifi(socket);
    //todo: review returning priority to normal.
    Network network("Public Adhoc Network");
    if(wifi.ssid_exists(network))
        wifi.set_variable(network, "priority", "0");

    string reply = wifi.send_command("AP_SCAN 1", NULL);
    return wifi.is_reply_ok(reply);
  }

int main(int argc, char** argv){
    int i=1;
    char* socket = new char[256];
    if(argc < 3)
        return 2;

    while(argc>1){
        if(!strcmp(argv[i], "-c")){
            strcpy(socket, argv[i+1]);
            --argc;
            if(connect_to_public_network(socket))
                return 0;
            else
                return 1;
        }
        if(!strcmp(argv[i], "-r")){
            strcpy(socket, argv[i+1]);
            --argc;
            if(return_ap_scan_and_priority_to_normal(socket))
                return 0;
            else
                return 1;
        }
        ++i;
        --argc;
    }
    return 3;
}