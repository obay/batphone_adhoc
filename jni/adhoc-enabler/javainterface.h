#ifndef JAVAINTERFACE_H
#define JAVAINTERFACE_H

#include </home/obay/bin/android-ndk-r11c/platforms/android-3/arch-arm/usr/include/jni.h>
/* Header for class WpaClient*/

#ifndef _Included_WpaClient
#define _Included_WpaClient
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jboolean JNICALL Java_WpaClient_connect_to_public_network
  (JNIEnv *, jobject, jstring);

JNIEXPORT jboolean JNICALL Java_WpaClient_return_ap_scan_to_normal
  (JNIEnv *, jobject, jstring);

#ifdef __cplusplus
}
#endif
#endif

#endif // JAVAINTERFACE_H
