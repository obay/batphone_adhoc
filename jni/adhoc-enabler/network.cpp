#include "network.h"

Network::Network(string ssid):
    ssid(ssid), priority(1), frequency(2412), mode(0), id(-1){}

void Network::print() const{
    debug("network info{");
    debug("\tid " + tostring(this->id));
    debug("\tssid " + this->ssid);
    debug("}");
}
void Network::print_verbose() const{
    debug("network info{");
    debug("\tid " + tostring(this->id));
    debug("\tssid " + this->ssid);
    debug("\tfrequency " + this->frequency);
    debug("\tmode " + tostring(this->mode));
    debug("\tpriority " + tostring(this->priority));
    debug("\tkey_mgmt " + this->key_mgmt);
//    debug("\tAP_SCAN " + tostring(this->ap_scan)); todo: move this line to main()
    debug("}");
}

// ************************ wifi manager aka wpa_client

void WifiManager::open_connection(){
    if(this->socket.length()==0)
        return;
    this->ctrl = wpa_ctrl_open(this->socket.c_str());
    if(this->ctrl == NULL){
        throw Errno_exception("Unable to connect to wpa_supplicant at socket/interface "+this->socket);
    }
}

string WifiManager::send_command(const string cmd, void (*msg_cb)(char *, size_t)) const{

    size_t reply_len = *(this->max_reply_len);
    int s = 0;
    s = wpa_ctrl_request(this->ctrl, cmd.c_str(), cmd.length(), this->_reply, &reply_len, msg_cb);
    string rep = string(this->_reply, reply_len);
    if(rep.find("UNKNOWN COMMAND") == string::npos){
        debug(cmd);
        debug(rep);
    }else{
        debug("UNKNOWN COMMAND: "+cmd);
    }

    if(s==0){
        return rep;
    }
    if(s==-1){
        throw logic_error("Command Failed!");
    }
    else if(s==-2){
        throw logic_error("Command Timeout!");
    }
    throw logic_error("Unknown reply from wpa_supplicant control interface");
}

void WifiManager::set_variable(Network network, string variable, string value) const{
    debug("will set " + variable + " with " + value);
    try{
        string cmd = string("SET_NETWORK ");
        cmd += tostring(network.id) + " " + variable + " " + value;
        this->send_command(cmd, NULL);
    }catch(exception &e){
        throw Variable_error_exception(variable, value);
    }
}

vector<Network> WifiManager::list_networks() const{
    this->send_command("SCAN", NULL);
    sleep(1);
    string reply = this->send_command("LIST_NETWORKS", NULL);
    string line;
    vector<Network> networks;

    size_t first_line_pos = reply.find('\n'); //to skip the header of the output.
    if(reply.length() <= first_line_pos) //there is no line after the header todo: make sure this work.
        return networks;

    for(uint k=first_line_pos+1; k<reply.length(); ++k){
        line = "";
        while(reply[k] != '\n' && k<reply.length())
            line += reply[k++];

        debug("line "+line);
        if(!custom_isdigit(line[0]))//first field is id. if it is not a number we reach end of output.
            break;
        string id, ssid, bssid, flags;
        int i = 0;

        while(line[i] != '\n' and line[i] != '\t' and line[i] != '\0') id += line[i++];
        ++i;
        while(line[i] != '\n' and line[i] != '\t' and line[i] != '\0') ssid += line[i++];
        ++i;
        while(line[i] != '\n' and line[i] != '\t' and line[i] != '\0') bssid += line[i++];
        ++i;
        while(line[i] != '\n' and line[i] != '\t' and line[i] != '\0') flags += line[i++];

        Network net(ssid);
        net.id = custom_stoi(id);
        net.bssid = bssid;
        net.flags = flags;

        networks.push_back(net);
    }
    return networks;
}

void WifiManager::create_network(Network& network) const{
    if(this->ssid_exists(network))
        return;
    string reply = this->send_command("ADD_NETWORK", NULL);
    try{
        network.id = custom_stoi(reply);
    }catch(invalid_argument e){
        throw logic_error("Can not create Network \""+network.ssid+"\"");
    }

    this->set_all_network_variables(network);
}

void WifiManager::update_network(Network &network) const{
    if(network.id >= 0 or this->bssid_exists(network) or this->ssid_exists(network))
        this->set_all_network_variables(network);
    else
        throw invalid_argument("Netowrk \"" + network.ssid + "\" does not exists");
}

WifiManager::WifiManager(string socket): socket(socket){
    //Follow RAII
    this->max_reply_len = new size_t(1024*4);
    this->_reply = new char[*this->max_reply_len];

    this->open_connection();
}

bool WifiManager::ssid_exists(Network& network)const{
    vector<Network> nets = this->list_networks();
    for(auto net : nets)
        if(net.ssid == network.ssid){
            network.id = net.id;
            return true;
        }
    return false;
}
bool WifiManager::bssid_exists(Network& network)const{
    vector<Network> nets = this->list_networks();
    for(auto net : nets)
        if(net.bssid == network.bssid){
            network.id = net.id;
            return true;
        }
    return false;
}

bool WifiManager::set_network_as_current(const Network& network) const{
    string reply;
    this->send_command(string("ENABLE_NETWORK " + tostring(network.id)), NULL);
    reply = this->send_command(string("SELECT_NETWORK " + tostring(network.id)), NULL);
    return is_reply_ok(reply);
}
bool WifiManager::is_reply_ok(const string& reply) const{
    if(reply.length() < 2)
        return false;
    if(reply[0] == 'O' && reply[1] == 'K')
        return true;
    return false;
}

WifiManager::~WifiManager(){
    wpa_ctrl_close(this->ctrl);
    debug("closed connection with wpa_supplicant.");
    delete this->max_reply_len;
    delete this->_reply;
}

void WifiManager::set_all_network_variables(Network network) const{
    this->set_variable(network, "ssid", network.ssid);
    this->set_variable(network, "bssid",tostring(network.bssid));
    this->set_variable(network, "frequency",tostring(network.frequency));
    this->set_variable(network, "mode",tostring(network.mode));
    this->set_variable(network, "key_mgmt", network.key_mgmt);
    this->set_variable(network, "priority",tostring(network.priority));
}


bool custom_isdigit(char& ch){
    if(ch>=48 && ch<=57)
        return true;
    else
        return false;
}

int custom_stoi(const std::string& str)
{
    int result = 0;
    bool ispositive = true;
    for(unsigned int i=0; i<str.length(); ++i){
        char c = str[i];
        if(custom_isdigit(c)){
            result *= 10;
            if(ispositive)
                result += c - 48;
            else
                result -= c - 48;
        }
        else if(i==0){
            if(c == '-')
                ispositive=false;
            else
                throw invalid_argument("custom_stoi");
        }
        else if(i==1 && str[0] == '-')//the first char was '-' but the second is not a number
            throw invalid_argument("custom_stoi");
        else
            return result;
    }
    return result;
}

void toupper(string &str){
    for(unsigned int i=0; i<str.length(); ++i)
        if(str[i]>= 97 && str[i] <= 122)
            str[i] -= 32;
}

void debug(const string& message){
    if(message.length())
        cout << message << endl;
    else
        cout << "Empty string" << endl;
}

Variable_error_exception::Variable_error_exception(string name, string value){
    string msg = string("Unable to set variable '");
    msg += name + "'' with value '" +value + "'";
    this->message = msg;
}

const char* Variable_error_exception::what() const throw (){
    return message.c_str();
}

Errno_exception::Errno_exception(string message){
    this->message = message + ". Error: " + strerror(errno);
}

const char* Errno_exception::what() const throw (){
    return message.c_str();
}
